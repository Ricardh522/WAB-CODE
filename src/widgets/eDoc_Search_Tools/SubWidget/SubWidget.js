define([
    "esri/config",
    "esri/domUtils",
    "esri/graphic",
    "esri/InfoTemplate",
    "esri/urlUtils",
    "esri/dijit/InfoWindowLite",
    "esri/geometry/Multipoint",
    "esri/geometry/Point",
    "esri/geometry/webMercatorUtils",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/layers/ArcGISImageServiceLayer",
    "esri/layers/FeatureLayer",
    "esri/symbols/PictureMarkerSymbol",
    "esri/request",
    "esri/arcgis/utils",

    "dojo/ready",
    'dojo/_base/declare',
    "dojo/_base/lang",
    "dojo/Evented",
    "dojo/_base/array",
    'dojo/dom-construct',
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/dom-class",
    "dojo/dom-form",
    "dojo/dom",
    "dojo/query",
    "dojo/html",
    "dojo/on",
    "dojo/json",
    "dojox/data/CsvStore",
    "dojox/encoding/base64",
    "dojo/parser",
    "dojo/topic",
    "dojo/request/xhr",
    "dojo/request/iframe",
    "dojo/promise/all",
    "dojo/Deferred",
    'dojo/has',

    "dstore/Memory",
    'dstore/legacy/DstoreAdapter',
    'dstore/Trackable',
    'dstore/Filter',
    'dstore/Rest',
    "dstore/RequestMemory",
    "dstore/SimpleQuery",

	'dgrid/OnDemandGrid',
    'dgrid/List',
	'dgrid/extensions/ColumnReorder',
	'dgrid/extensions/ColumnResizer',
	'dgrid/extensions/ColumnHider',
	'dgrid/Keyboard',
	'dgrid/Selection',
    'dgrid/editor',
	'dgrid/extensions/DnD',
	'dojo/dnd/Source',
	'dgrid/extensions/DijitRegistry',

    "dijit/registry",
    "dijit/Dialog",
    "dijit/layout/BorderContainer",
    "dijit/layout/TabContainer",
    "dijit/layout/ContentPane",
    "dijit/layout/AccordionContainer",
    "dijit/form/Button",
    "dijit/form/FilteringSelect",
    'dijit/form/ComboBox',
    "dijit/form/CheckBox",
    "dijit/form/ToggleButton",
    "dijit/form/Form",

    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',

    "dojo/text!./templates/SubWidget.html",

    "dojo/NodeList-traverse"

], function (
    esriConfig,
    domUtils,
    Graphic,
    InfoTemplate,
    urlUtils,
    InfoWindowLite,
    Multipoint,
    Point,
    webMercatorUtils,
    ArcGISDynamicMapServiceLayer,
    ArcGISImageServiceLayer,
    FeatureLayer,
    PictureMarkerSymbol,
    esriRequest,
    arcgisUtils,

    ready,
    declare,
    lang,
    Evented,
    Array,
    domConstruct,
    domAttr,
    domStyle,
    domClass,
    domForm,
    dom,
    query,
    html,
    on,
    JSON,
    CsvStore,
    base64,
    parser,
    topic,
    xhr,
    iframe,
    all,
    Deferred,
    has,

    Memory,
    DstoreAdapter,
    Trackable,
    Filter,
    Rest,
    RequestMemory,
    SimpleQuery,

    OnDemandGrid,
    List,
    ColumnReorder,
    ColumnResizer,
    ColumnHider,
    Keyboard,
    Selection,
    editor,
    DnD,
    DnDSource,
    DijitRegistry,

    registry,
    Dialog,
    BorderContainer,
    TabContainer,
    ContentPane,
    AccordionContainer,
    Button,
    FilteringSelect,
    ComboBox,
    CheckBox,
    ToggleButton,
    Form,

    _WidgetBase,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,

    template

) {
//    'use strict';
    var backend_url = "http://edoc.azurewebsites.net/eDoc";

    var TrackableRest = new declare([Rest, SimpleQuery, Trackable]);
    var fileRestStore = new TrackableRest({
        name: "fileRestStore",
        target: backend_url + '/files/',
        useRangeHeaders: true,
        ascendingPrefix: '',
        sortParam: 'ordering',
        idProperty: 'pk'
    });
    fileRestStore.track();


    var assignmentRestStore = new TrackableRest({
        name: "assignmentRestStore",
        target: backend_url + '/assignments/',
        useRangeHeaders: true,
        sortParam: 'ordering',
        ascendingPrefix: '',
        idProperty: 'pk'
    });
    assignmentRestStore.track();


    var createColumns = function() {
        return [
                {
                    field: 'pk',
                    label: 'Primary Key',
                    reorderable: false,
                    resizable: true,
                    hidden: true,
                    unhidable: false
                },
                {
					field: 'base_name',
					label: 'File Name',
					reorderable: false,
					resizable: true,
                    hidden: false,
					unhidable: true
				},
				{
					field: 'file_type',
					label: 'File Type',
					reorderable: true,
					resizable: true,
					hidden: true,
					unhidable: false
				},
				{
					field: 'size',
					label: 'File Size',
					reorderable: true,
					resizable: true,
					hidden: true,
					unhidable: false
				},
				{
					field: 'grid_cells',
					label: 'Grid Cell(s)',
					reorderable: true,
					resizable: true,
					hidden: false
				},
				editor({
					field: 'comment',
					editor: 'TextBox',
					editOn: has('touch') ? 'click' : 'dblclick',
                    autoSelect: true,
                    autoSave: true,
					label: 'Comments',
					reorderable: true,
					resizable: true,
					hidden: false
				}),
				{
					field: 'date_added',
					label: 'Date Added',
					reorderable: true,
					resizable: true,
					hidden: true
				},
				{
					field: 'mime',
					label: 'MIME Type',
					reorderable: true,
					resizable: true,
					hidden: true
				},
				{
					field: 'file_path',
					label: 'File Path',
					reorderable: true,
					resizable: true,
					hidden: true
				}
            ];
    };

    var myFileMemory = declare([Memory, Trackable]);

    var myAssignMemory = declare([Memory, Trackable]);

    var myFileNameMemory = declare([Memory, Trackable]);

    var myFileTypeMemory = declare([Memory, Trackable]);

    var myGridCellMemory = declare([Memory, Trackable]);

    var myFileSelectionMemory = declare([Memory, Trackable]);

    var createMemStore = function(store, data, idProperty) {
        var newstore = new store({
            data: data,
            idProperty: idProperty
        });
        return newstore;
    };

    var createFileNameFixture = function (inputArray) {
        var fileNames =[];
        if (inputArray.length) {
            var nameCompare = [];
            Array.forEach(inputArray, function (e) {
                if(Array.indexOf(nameCompare, e.base_name) === -1) {
                    nameCompare.push(e.base_name);
                    fileNames.push({
                        'base_name': e.base_name
                    });
                }
            });
            return fileNames;
        } else {
            fileNames = [{'base_name': "Empty"}];
            return fileNames;
        }
    };

    var createFileTypeFixture = function (inputArray) {
        var fileTypes = [];
        if (inputArray.length) {
            var typeCompare = [];
            Array.forEach(inputArray, function (e) {
                if (Array.indexOf(typeCompare, e.file_type) === -1) {
                    typeCompare.push(e.file_type);
                    fileTypes.push({
                        'file_type': e.file_type,
                        'checked': true
                    });
                }
            });
            return fileTypes;
        } else {
            fileTypes = [{"file_type": "Empty"}];
            return fileTypes;
        }
    };

    var createGridCellFixture = function (assignArray) {
        var gridCells = [];
        var temp = [];
        if (assignArray.length) {
            Array.forEach(assignArray, function (e) {

                if (Array.indexOf(gridCells, e.grid_cell) === -1) {
                    gridCells.push(e.grid_cell);
                    temp.push({'grid_cell': e.grid_cell});
                }
            });
            return temp;
        } else {
            temp = [{"grid_cell": "Empty"}];
            return temp;
        }
    };


    var updateFileTypeCount = function(typeStore, count, label) {

        var target = typeStore.getSync(label);
        if (count === 0) {
            count = false;
        }

        target.checked = count;
        typeStore.putSync(target);
    };


    var getCount = function(attribute, value, array) {
        var iters = Array.map(array, function(item) {
            if (item[attribute] === value) {
                return 1;
            } else {
                return 0;
            }
        });

        var passes = Array.filter(iters, function(item) {
            return item;
        });

        return passes.length;
    };

    var buildFileTypeButtons = function(collection, filteredFileArray) {

        var typeNode = domConstruct.create("div");
        var mainDeferred = new Deferred();
        collection.fetch().then(function(arr) {
            // for each type in the typeStore create a button
            var boxes = Array.map(arr, function (item) {
                var deferred = new Deferred();
                if (registry.byId(item.file_type) === undefined) {
                    var file_type = item.file_type;
                    // get the count in the filtered file Array
                    var count = getCount("file_type", file_type, filteredFileArray);
                    // update the typeStore checked properties with the type count
                    updateFileTypeCount(collection, count, file_type);
                    var cbox = new ToggleButton({
                        iconClass: "dijitCheckBoxIcon",
                        label: file_type+" ("+count+")" ,
                        id: file_type,
                        // a zero count is false, while all others are truthy
                        checked: count,
                        onChange: function(val) {
                            // this sets the checked attribute on the typeStore when the button is unchecked or checked
                            updateFileTypeCount(collection, val, file_type);
                            topic.publish("fileTypeMemory_updated");
                        }
                    });


                    cbox.placeAt(typeNode, "last");

                    return deferred.resolve();
                } else {
                    return deferred.resolve();
                }
            });

            all(boxes).then(function() {
                mainDeferred.resolve(typeNode);
            });
        });


        return mainDeferred.promise;
    };

    var pollSelection = function(selectStore) {

        selectStore.fetch().then(function(arr) {
            Array.forEach(arr, function(e) {
                var box = registry.byId(e.pk + '_box');
                // if checkbox widget does not exist in the registry create it
                if (box === undefined) {
                    var myDiv = domConstruct.create("div", {
                        id: e.pk + "_input"
                    });

                    var chk = new CheckBox({
                        id: e.pk + '_box',
                        name: e.pk + '_box',
                        checked: true,
                        value: backend_url + "/io/" + e.pk + "/_download/",
                        onChange:  function(b) {
                            // set the checked property to match the checkbox status
                            selectStore.get(e.pk).then(function(e) {
                                e.checked = b;
                                selectStore.put(e);
                            });
                        }
                    }, "chk");

                    chk.placeAt(myDiv);

                    var lbl = domConstruct.create("label", {
                        "for": chk.name,
                        innerHTML: e.base_name
                    });

                    myDiv.appendChild(lbl);
                    chk.startup();

                    domConstruct.place(myDiv, registry.byId('_downloadForm').domNode, "last");

                } else {
                    // set the checked property to match what is set in the selectionStore
                    var checked = e.checked;
                    box.set("checked", checked);
                }
            });
        });
    };

    var buildActionButtons = function(selectionStore) {
        // create download button
        var mainDeferred = new Deferred();
        var downloadNode = domConstruct.create("div", {
        });
        var buttonNames = ["Add to Basket", "Unselect All", "Clean", "Select All", "Download Selected"];
        var grid = registry.byId("main_dgrid");
        var selectStore = selectionStore;

        Array.forEach(buttonNames, function(e) {
            var btn = new Button({
                label: e,
                class: 'action_button',
                onClick: function() {
                    switch(e) {
                        case "Add to Basket":
                            var selection = Object.keys(grid.selection);
                            Array.forEach(selection, function(e) {
                                // check if e.pk is already in the
                                var _pk = grid.row(e).data.pk;
                                var _base_name = grid.row(e).data.base_name;
                                // if the selected file is not already in the selectionStore, add it
                                selectStore.get(_pk).then(function(e) {
                                    if (e === undefined) {
                                        selectStore.add({
                                            'pk': _pk,
                                            'base_name': _base_name,
                                            'checked': true
                                        });
                                    }
                                });
                            });

                            // create the checkboxes after all the entries have been added to the selectionStore
                            pollSelection(selectStore);
                            break;

                        case "Unselect All":
                            // set the checked property of each checkbox widget to be false
                            selectStore.forEach(function(entry) {
                                var box = registry.byId(entry.pk + '_box');
                                box.set("checked", false);
                                entry.checked = false;
                            });
                            break;

                        case "Select All":
                            selectStore.forEach(function(entry) {
                                var box = registry.byId(entry.pk + '_box');
                                box.set("checked", true);
                                entry.checked = true;
                            });
                            break;

                        case "Clean":
                            // obtain the checked status from the selectionStore rather than from the domNodes, triggers the onDelete event in setupConnections
                            selectStore.filter({checked: false}).forEach(function(entry) {
                                selectStore.remove(entry.pk).then(function(event) {
                                    console.log(entry + " was removed from the selections store " + event);
                                });
                            });
                            break;

                        case "Download Selected":
                            var formId = '_downloadForm';
                            var formJson = JSON.parse(domForm.toJson(formId));


                            var keys = Object.keys(formJson);
                            var urls = Array.map(keys, function(key) {
                                var deferred2 = new Deferred();
                                if (formJson[key].length) {
                                    deferred2.resolve(formJson[key]);
                                } else {
                                    deferred2.resolve([]);
                                }
                                return deferred2.promise;
                            });

                            all(urls).then(function(url_list) {
                                Array.forEach(url_list, function(e) {
                                    if (e.length) {
                                        var a = domConstruct.create("a", {
                                            href: e,
                                            download: true
                                        });
                                        on.emit(a, 'click', {
                                            bubble: false,
                                            cancelable: true
                                        });
                                        domConstruct.destroy(a);
                                    }
                                });
                            });
                            break;
                    }
                    selectStore.fetch().then(
                        function(arr) {
                            localStorage.setItem('basketArray', JSON.stringify(arr));
                        });
                    }
            });

            btn.placeAt(downloadNode,"last");
            mainDeferred.progress(downloadNode);
        });


        var myForm = new Form({
            id: "_downloadForm",
            class: "download-form"
        });

        // The selectStore is initialized with data from localStorage
        // this function will rehydrate the basket upon creation of the
        // widget.
        pollSelection(selectStore);

        myForm.placeAt(downloadNode);

        mainDeferred.resolve(downloadNode);

        return mainDeferred.promise;
    };

    var assignCell2File = function(backend_url, grid, grid_cell, assignmentRestStore) {
        var mainDeferred = new Deferred();
        var selection = Object.keys(grid.selection);
        var updates = Array.map(selection, function(e) {
            var deferred = new Deferred();
            var _pk = grid.row(e).data.pk;


            var options = {
                usePost: true
            };

            var request = {
                url: backend_url + "/assignments/",
                handleAs: 'json',
                content: {
                    "grid_cell": grid_cell,
                    "file": backend_url + "/files/" + _pk + "/"
                }
            };

            esriRequest(request, options).then(function(e) {
                return deferred.resolve({"status": "success"});
            }, function(err) {
                console.log(err);
                return deferred.resolve({"status": "error"});
            });
        });

        all(updates).then(function(results) {
            console.log("updates posted to AssignmentRestStore :: " + results);
            assignmentRestStore.emit('add', {});
            mainDeferred.resolve(results);
        });
        return mainDeferred.promise;
    };

    var removeCellFromFile = function(backend_url, grid, mainAssignmentMemoryStore, grid_cell, assignmentRestStore) {
        var mainAssign = mainAssignmentMemoryStore;
        var mainDeferred = new Deferred();
        var selection = Object.keys(grid.selection);
        var deletes = Array.map(selection, function(e) {
            var deferred = new Deferred();
            var _pk = grid.row(e).data.pk;
            var file_url = backend_url + "/files/" + _pk + "/";
            var cell_filter = new mainAssign.Filter();
            var pk_filter = new mainAssign.Filter();

            var removalFilter = cell_filter.eq('grid_cell', grid_cell).and(pk_filter.eq('file', file_url));

            mainAssign.filter(removalFilter).fetch().then(function(array) {
                var length = array.length;
                if (length === 1) {
                    var assign_pk = array[0].pk;
                    xhr(backend_url + "/assignments/"+assign_pk+"/_clean/", {
                        handleAs: "json",
                        method: "GET"
                    }).then(function(e) {
                        return deferred.resolve({"status": "success"});
                    }, function(err) {
                        console.log(err);
                        return deferred.resolve({"status": "error"});
                    });
                }
            }, function(err) {
                console.log(err);
            });

        }, function(err) {
            console.log(err);
        });

        all(deletes).then(function(results) {
            console.log("updates posted to AssignmentRestStore :: " + results);
            assignmentRestStore.emit('add', {});
            mainDeferred.resolve(results);
        });
        return mainDeferred.promise;
    };


    var updateFilteredDisplay = function(mainGrid, mainFileMemoryStore, mainAssignmentMemoryStore, filters, nameStore, typeStore, gridStore) {
        var mainDeferred = new Deferred();
        var deferred1 = new Deferred();
        var deferred2 = new Deferred();

        var deferreds = [deferred1, deferred2];

        var mainMemory = mainFileMemoryStore;
        var mainAssign = mainAssignmentMemoryStore;

        var newAssigns = mainAssign.filter(filters.assignCellFilter.and(filters.assignNameFilter));
        newAssigns.fetch().then(function(assignArray) {

                var names = Array.map(assignArray, function(e) {
                    var deferred3 = new Deferred();
                    deferred3.resolve(e.base_name);
                    return deferred3.promise;
                });

                all(names).then(function(arr) {

                    if (filters.assignCellFilter.args[1] !== "") {
                    // rebuild the nameFilter by querying the assignStore for grid cell matches

                        var fileNameFilter = new mainFileMemoryStore.Filter();
                        var newFileNameFilter = fileNameFilter.in('base_name', arr);
                        filters.fileNameFilter = newFileNameFilter;
                    }
                    var newColl = mainMemory.filter(filters.fileNameFilter.and(filters.fileTypeFilter));

                    mainGrid.set("collection", newColl);
                    newColl.fetch().then(function(array) {
                        // create a new grid cell filter based upon the newCollection of files
                        var file_names = Array.map(array, function(item) {
                            return item.base_name;
                        });
                        var assignFilter = new mainAssignmentMemoryStore.Filter();
                        var newAssignNameFilter = assignFilter.in('base_name', file_names);
                        filters.assignNameFilter = newAssignNameFilter;
                        var assignFilter2 = new mainAssignmentMemoryStore.Filter();
                        var newAssignCellFilter = assignFilter2.ne('grid_cell', '');
                        filters.assignCellFilter = newAssignCellFilter;

                        var assignColl = mainAssignmentMemoryStore.filter(filters.assignNameFilter.and(filters.assignCellFilter));
                        assignColl.fetch().then(function(e) {
                            deferred1.resolve(array);
                            deferred2.resolve(e);
                        });
                    });
                });
        });


        all(deferreds).then(function(arr) {
            var fileArray = arr[0];
            var assignArray = arr[1];

            rebuildMembers(fileArray, assignArray, nameStore, typeStore, gridStore).then(function(e) {
                mainGrid.refresh();
                mainGrid.renderArray(fileArray);
                topic.publish('mainGrid_refresh', {fileArray: fileArray});
                mainDeferred.resolve(arr);
            });
        });
        // the filtered fileArray and assignArray are returned in the promise
        return mainDeferred.promise;
    };

    var resetMainFileMemoryStore = function (mainFileMemoryStore, fileRestStore, typeStore) {
        var deferred = new Deferred();
        var mainMemory = mainFileMemoryStore;
        fileRestStore.fetch().then(function (arr) {
            // the mainFileMemory Store and the typeStore are set with all of the data from the FileRest Store
            mainMemory.setData(arr);
            typeStore.setData(createFileTypeFixture(arr));
            deferred.resolve(arr);

        });
        // returns the un-filtered fileArray in the promise
        return deferred.promise;
    };

    var resetMainAssignmentMemoryStore = function(mainAssignmentMemoryStore, assignRestStore) {
        var deferred = new Deferred();
        assignRestStore.fetch().then(function (arr) {
            mainAssignmentMemoryStore.setData(arr);
            deferred.resolve(arr);
        });
        // return the un-filtered assignmentStore in the promise
        return deferred.promise;
    };

     var fetchRestStores = function(fileRestStore, assignRestStore, mainFileMemoryStore, mainAssignmentMemoryStore, typeStore) {
        var deferred = new Deferred();
        var deferred1 = new Deferred();
        var deferred2 = new Deferred();

        var deferreds = [deferred1, deferred2];
        resetMainFileMemoryStore(mainFileMemoryStore, fileRestStore, typeStore).then(function(arr) {
            deferred1.resolve(arr);
        });

        resetMainAssignmentMemoryStore(mainAssignmentMemoryStore, assignRestStore).then(function(arr) {
            deferred2.resolve(arr);
        });


        all(deferreds).then(function(arr) {
            deferred.resolve(arr);
        });

        return deferred.promise;
    };

    var cleanFilters = function(filterObjects, mainFileMemoryStore, mainAssignmentMemoryStore) {

        var fileNameFilter = new mainFileMemoryStore.Filter();
        filterObjects.fileNameFilter = fileNameFilter.ne('base_name', "");
        var fileTypeFilter = new mainFileMemoryStore.Filter();
        filterObjects.fileTypeFilter = fileTypeFilter.ne('file_type', "");
        var fileCellFilter = new mainFileMemoryStore.Filter();
        filterObjects.fileCellFilter = fileCellFilter.ne('grid_cells', "['_']");

        var assignCellFilter = new mainAssignmentMemoryStore.Filter();
        filterObjects.assignCellFilter = assignCellFilter.ne('grid_cell', "");
        var assignNameFilter = new mainAssignmentMemoryStore.Filter();
        filterObjects.assignNameFilter = assignNameFilter.ne('base_name', "");

        return filterObjects;
    };

    var rebuildMembers = function(fileArray, assignArray, nameStore, typeStore, gridStore) {
        // rebuilding Members will set the localStorage arrays (file, and assign), setData on the name, type, and grid Stores.
        var mainDeferred = new Deferred();

        localStorage.setItem('fileArray', JSON.stringify(fileArray));
        localStorage.setItem('assignArray', JSON.stringify(assignArray));

        // these memory stores hold the filtered names and grids for the views
        nameStore.setData(createFileNameFixture(fileArray));
        gridStore.setData(createGridCellFixture(assignArray));

        try {
            // all of the fileType buttons are destroyed
            if (registry.byId('_fileTypePane') !== undefined) {
                var pane = registry.byId('_fileTypePane');
                var nodes = registry.findWidgets(pane.domNode);

                var deletes = Array.map(nodes, function(e) {
                    var deferred = new Deferred();
                    if (registry.byId(e.id)) {
                        registry.remove(e.id);
                        e.destroyRecursive();
                        deferred.resolve();
                    } else {
                        deferred.resolve();
                    }
                    return deferred.promise;
                });

                // domConstruct.empty('_fileTypePane');

                all(deletes).then(function(e) {
                    // update fileType buttons from the typeStore
                    var filteredTypes = createFileTypeFixture(fileArray);
                    buildFileTypeButtons(typeStore, fileArray).then(function(buttons) {
                        pane.setContent(buttons);
                        mainDeferred.resolve(buttons);
                    });
                });
            }
        }
        catch(e) {
            var text = "unable to rebuild the buttons within the rebuild members function :: "+e;
            console.log(text);
            mainDeferred.resolve(text);
        }

        return mainDeferred.promise;
    };

    var renewFromRest = function(fileRestStore, assignmentRestStore, mainFileMemoryStore, mainAssignmentMemoryStore, typeStore,
        mainGrid, eDocFilters, nameStore, gridStore) {
        var mainDeferred = new Deferred();
        // retrieve the fileArray and the assignArray from the Rest Stores
        fetchRestStores(fileRestStore, assignmentRestStore, mainFileMemoryStore, mainAssignmentMemoryStore, typeStore).then(function(e) {
            // using the current state of the filters, render the grid, then return the filtered fileArray and assignArray
            console.log("fetched rest stores scoped within 'renewFromRest'");
            updateFilteredDisplay(mainGrid, mainFileMemoryStore, mainAssignmentMemoryStore, eDocFilters, nameStore, typeStore, gridStore).then(function(arr) {
                console.log("Updated Filtered Display scoped from within 'renewRest'");
                mainDeferred.resolve(arr);
            });
        });
        return mainDeferred.promise;
    };

    return declare([_WidgetBase,  _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {

        templateString: template,
        baseClass: 'sub-widget',
        widgetsInTemplate: true,
        id: "eDoc",

        constructor: function(options) {
            this.inherited(arguments);
            if (options != null && options.hasOwnProperty('map')) {
                    this.map = options.map;
                }

        },

        postCreate: function () {

            var self = this;
            this.inherited(arguments);

            console.log('widgets._eDock.SubWidget::postCreate', arguments);

            this.fileRestStore = fileRestStore;
            this.assignmentRestStore = assignmentRestStore;
            var backend_url = this.backend_url = backend_url;
            this.createColumns = createColumns;
            this.createMemStore = createMemStore;
            this.createFileNameFixture = createFileNameFixture;
            this.createFileTypeFixture = createFileTypeFixture;
            this.createGridCellFixture = createGridCellFixture;
            this.updateFileTypeCount = updateFileTypeCount;
            this.getCount = getCount;
            this.buildFileTypeButtons = buildFileTypeButtons;
            this.pollSelection = pollSelection;
            this.buildActionButtons = buildActionButtons;
            this.assignCell2File = assignCell2File;
            this.removeCellFromFile = removeCellFromFile;
            this.updateFilteredDisplay = updateFilteredDisplay;
            this.resetMainAssignmentMemoryStore = resetMainAssignmentMemoryStore;
            this.resetMainFileMemoryStore = resetMainFileMemoryStore;
            this.fetchRestStores = fetchRestStores;
            this.cleanFilters = cleanFilters;
            this.rebuildMembers = rebuildMembers;
            this.renewFromRest = renewFromRest;

            // rehydrate the memory store eventually
            var fileArray;
            if (localStorage.fileArray === undefined) {
                fileArray =  [{
                    "pk": 1,
                    "base_name": "Retreiving Data from Server...",
                    "file_type": "...",
                    "size": "...",
                    "grid_cells": "...",
                    "comment": "...",
                    "date_added": "...",
                    "mime": "...",
                    "file_path": "...",
                    }];

            } else {
                   fileArray = JSON.parse(localStorage.fileArray);
            }

            var assignArray;
            if (localStorage.assignArray === undefined) {
                assignArray =[{
                    pk: 1,
                    grid_cell: "Retreiving Data from Server...",
                    file_path: "...",
                    comment: "...",
                    date_assigned: "..."
                               }];
            } else {
                 assignArray = JSON.parse(localStorage.assignArray);
            }

            var basketArray;
            if (localStorage.basketArray === undefined) {
                basketArray = [{
                    pk: 1,
                    base_name: "Empty Basket",
                    checked: false
                }];
            } else {
                basketArray = JSON.parse(localStorage.basketArray);
            }

            var mainFileMemoryStore = this.mainFileMemoryStore = createMemStore(myFileMemory, fileArray, 'pk');
            mainFileMemoryStore.track();
            var mainAssignmentMemoryStore = this.mainAssignmentMemoryStore = createMemStore(myAssignMemory, assignArray, 'pk');
            mainAssignmentMemoryStore.track();

            var customGrid = declare([OnDemandGrid, Keyboard, Selection, editor, ColumnReorder, ColumnResizer, ColumnHider, DijitRegistry]);

            var mainGrid = self.mainGrid = new customGrid({
                collection: mainFileMemoryStore,
                id: "main_dgrid",
			    loadingMessage: 'Loading Data',
                noDataMessage: 'No results found.',
			    selectionMode: "extended",
                dndSourceType: "grid-row",
			    allowSelectAll: true,
                cellNavigation: true,
                pageSkip: 15,
			    columns: createColumns()
            });


            var fileNameFilter = self.fileNameFilter = new mainFileMemoryStore.Filter();
            var newFileNameFilter = fileNameFilter.ne('base_name', "");
            var fileTypeFilter = self.fileTypeFilter = new mainFileMemoryStore.Filter();
            var newFileTypeFilter = fileTypeFilter.ne('file_type', "");

            var assignCellFilter = new mainAssignmentMemoryStore.Filter();
            var newAssignCellFilter = assignCellFilter.ne('grid_cell', "");
            var assignNameFilter = new mainAssignmentMemoryStore.Filter();
            var newAssignNameFilter = assignNameFilter.ne('base_name', "");

            var filterObjects = self.filterObjects = {
                'fileNameFilter': newFileNameFilter,
                'fileTypeFilter': newFileTypeFilter,

                'assignCellFilter': newAssignCellFilter,
                'assignNameFilter': newAssignNameFilter
            };



            // Create the Filtered Memory Stores that hold the filtered fileArray and assignArray
            var nameStore = self.nameStore = createMemStore(myFileNameMemory, createFileNameFixture(fileArray), 'base_name');
            nameStore.track();
            var typeStore = self.typeStore = createMemStore(myFileTypeMemory, createFileTypeFixture(fileArray), 'file_type');
            typeStore.track();
            var gridStore = self.gridStore = createMemStore(myGridCellMemory, createGridCellFixture(assignArray), 'grid_cell');
            gridStore.track();

            // the selection store represents selected rows in the grid
            var selectionStore = self.selectionStore = createMemStore(myFileSelectionMemory, basketArray, 'pk');


            var fileAdaptorStore = self.fileAdaptorStore = new DstoreAdapter(nameStore);
            var gridAdaptorStore = self.gridAdaptorStore = new DstoreAdapter(gridStore);


/////////////////////////////////////////////////////////////////////////////////////

            var _comboBoxFile = self._comboBoxFile = new ComboBox({

                    placeholder: "type here",
                    pageSize: 15,
                    name: "base_name",
                    store: fileAdaptorStore,
                    searchAttr: "base_name",
                    id: "_comboBoxFile"
                });

            var fileSearchPane = new ContentPane({
                content: _comboBoxFile,
                title: "Filename"
            });

            fileSearchPane.addChild(new Button({
                label: "Search",
                onClick: function () {
                    var inputString = _comboBoxFile.displayedValue;
                    // set both the fileFilters and the assignmentFilters
                    var filter = new mainFileMemoryStore.Filter();
                    var reg = new RegExp("^" + inputString);
                    filterObjects.fileNameFilter = filter.match('base_name', reg);

                    var filter2 = new mainAssignmentMemoryStore.Filter();
                    var reg2 = new RegExp("^" + inputString);
                    filterObjects.assignNameFilter = filter2.match('base_name', reg);

                    updateFilteredDisplay(mainGrid, mainFileMemoryStore, mainAssignmentMemoryStore, filterObjects, nameStore, typeStore, gridStore).then(function(e) {
                        console.log("file search complete");
                    });
                }
            }));


            fileSearchPane.addChild(new Button({
                label: "Clear",
                onClick: function () {
                    registry.byId('_comboBoxFile').set("displayedValue", "");
                    registry.byId('_comboBoxGrid').set("displayedValue", "");
                }
            }));

            fileSearchPane.addChild(new Button({
                label: "Reset",
                onClick: function () {
                    registry.byId('_comboBoxFile').set("displayedValue", "");
                    filterObjects = cleanFilters(filterObjects, mainFileMemoryStore, mainAssignmentMemoryStore);
                    updateFilteredDisplay(mainGrid, mainFileMemoryStore, mainAssignmentMemoryStore, filterObjects, nameStore, typeStore, gridStore).then(function(e) {
                        console.log("grid reset complete");
                    });
                }
            }));

          /////////////////////////////////////////////////////////////////


            var _comboBoxGrid = self._comboBoxGrid = new ComboBox({
                placeholder: 'select in map',
                pageSize: 25,
                name: "grid_cell",
                store: gridAdaptorStore,
                searchAttr: "grid_cell",
                id: '_comboBoxGrid'
            });

            var gridSearchPane = new ContentPane({
                content: _comboBoxGrid,
                title: "Grid Cell",
                selected: false
            });

            gridSearchPane.addChild(_comboBoxGrid);
            gridSearchPane.addChild(new Button({
                    label: "Search",
                    onClick: function () {
                        _comboBoxFile.set("displayedValue", "");
                        var item = _comboBoxGrid.get('displayedValue');
                        if (item.length) {
                            var assignStore = self.mainAssignmentMemoryStore;
                            var _filter = new assignStore.Filter();
                            var reg = new RegExp("^" + item);
                            filterObjects.assignCellFilter = _filter.match('grid_cell', reg);
                            updateFilteredDisplay(mainGrid, mainFileMemoryStore, mainAssignmentMemoryStore, filterObjects, nameStore, typeStore, gridStore).then(function(e) {
                                console.log("grid search complete");
                            });
                        } else {
                            console.log("There isn't any data in the grid_cell search box");
                        }
                    }
                }));

            gridSearchPane.addChild(new Button({
                    label: "Clear",
                    onClick: function () {
                        _comboBoxGrid.set("displayedValue", "");
                        _comboBoxFile.set("displayedValue", "");
                    }
                }));

            gridSearchPane.addChild(new Button({
                    label: "Reset",
                    onClick: function () {
                        _comboBoxGrid.set("displayedValue", "");
                        _comboBoxFile.set("displayedValue", "");
                        filterObjects = cleanFilters(filterObjects, mainFileMemoryStore, mainAssignmentMemoryStore);
                        updateFilteredDisplay(mainGrid, mainFileMemoryStore, mainAssignmentMemoryStore, filterObjects, nameStore, typeStore, gridStore).then(function(e) {
                            console.log("grid reset complete");
                        });
                       }
                }));

            gridSearchPane.addChild(new Button({
                label: "Assign to Selected File(s)",
                onClick: function() {
                    // get row from table and assign current displayed cell
                    var value = _comboBoxGrid.get("displayedValue");
                    if (value !== "") {
                        assignCell2File(backend_url, mainGrid, value, assignmentRestStore);
                    }
                }
            }));

            gridSearchPane.addChild(new Button({
                label: "Remove from Selected File(s)",
                onClick: function() {
                    // get row from table and assign current displayed cell
                    var value = _comboBoxGrid.get("displayedValue");
                    if (value !== "") {
                        try {
                            removeCellFromFile(backend_url, mainGrid, mainAssignmentMemoryStore, value, assignmentRestStore);
                        } catch (err) {
                            console.log('error in clearing the assignment :: '+err);
                        }
                    } else {
                        alert('a grid cell must be selected first');
                    }
                }
            }));

            var apFilters = new AccordionContainer({
                style: "width: 100%; height: 40%",
                region: 'top',
                id: 'apFilters',
                splitter: true
            });


            apFilters.addChild(fileSearchPane);
            apFilters.addChild(gridSearchPane);

 //////////////////////////////////////////////////////////////////////////////

            var fileTypePane = new ContentPane({
                 title: "File Type",
                 id: "_fileTypePane",
                 style: "height: 100%: width: 100%",
                 selected: true
             });

            apFilters.addChild(fileTypePane);
/////////////////////////////////////////////////////////////////////////////////
            // Buttons for selecting and managingdownloading files
            var actionButtons;
            buildActionButtons(selectionStore).then(function(buttons) {
                actionButtons = buttons;
            });

            var actionButtonPane = new ContentPane({
                title: "Action Buttons",
                id: "_actionButtonPane",
                style: "height: 100%: width: 100%",
                content: actionButtons
            });

            apFilters.addChild(actionButtonPane);

            var gridPane = new ContentPane({
                region: 'center',
                id: 'gridPane',
                content: mainGrid
            });


            registry.byId('bcMain').addChild(apFilters);
            registry.byId('bcMain').addChild(gridPane);

            // Hydrate the grid view with data from localStorage on startup
            updateFilteredDisplay(mainGrid, mainFileMemoryStore, mainAssignmentMemoryStore, filterObjects, nameStore, typeStore, gridStore).then(function(e) {
                console.log("initial update of filtered display complete");
            });

            this.setupConnections();
                //////////////////////////////////////////////////////////////////////
        },

        setupConnections: function () {
            var eDocFilters = this.filterObjects;
            this.inherited(arguments);
            var mainGrid = this.mainGrid;
            var typeStore = this.typeStore;
            var nameStore = this.nameStore;
            var gridStore = this.gridStore;
            var mainFileMemoryStore = this.mainFileMemoryStore;
            var mainAssignmentMemoryStore = this.mainAssignmentMemoryStore;
            var selectionStore = this.selectionStore;
            var fileRestStore = this.fileRestStore;
            var assignmentRestStore = this.assignmentRestStore;

            var self = this;
            console.log('widgets._eDock.SubWidget::setupConnections', arguments);

            renewFromRest(fileRestStore, assignmentRestStore, mainFileMemoryStore, mainAssignmentMemoryStore, typeStore,
                    mainGrid, eDocFilters, nameStore, gridStore);

            fileRestStore.on('add, update, delete', function(event) {
                console.log("files have been added, updated, or deleted to the server");
                renewFromRest(fileRestStore, assignmentRestStore, mainFileMemoryStore, mainAssignmentMemoryStore, typeStore,
                    mainGrid, eDocFilters, nameStore, gridStore);
            });


            assignmentRestStore.on('add, update, delete', function(event) {
                console.log("assignments have been added, updated, or deleted from the assignment store on server");
                renewFromRest(fileRestStore, assignmentRestStore, mainFileMemoryStore, mainAssignmentMemoryStore, typeStore,
                    mainGrid, eDocFilters, nameStore, gridStore);
            });


            topic.subscribe("mainGrid_refresh", function(event) {
                // this event will be used to read from the current grid view and rebuild the nameStore and gridStore.
                //The fileTypeStore gets modified with a disabled attribute if it is not in the grid view
    //             mainGrid.collection.select('file_type').fetch().then(function(types) {

    //                 var btn = query('button', '_fileTypePane');
    //                 btn.forEach(function(b) {
    // //                    var button = registry.byId(item.file_type);
    //                     if (types.indexOf(b.id) === -1) {
    //                         // this will check or uncheck the buttons
    //                         // disable or enable the buttons too
    //                         b.set('checked', false);
    //                         domClass.add(b, 'disabled');
    //                     } else {
    //                         b.set('checked', true);
    //                         domClass.remove(b, 'disabled');
    //                     }
    //                 });
    //             });
            });

            selectionStore.on('delete', function(event) {
                // destroy checkbox widget that was removed
                console.log(event.target+" has been removed from the selection Store");
                domConstruct.destroy(event.target.pk + "_input");
                registry.byId(event.target.pk + "_box").destroy();
            });

//            selectionStore.on('add', function(event) {
//                pollSelection(selectionStore);
//            });

            nameStore.on('add, update, delete', function(event) {
                alert("file_names have been altered in the filename Memory Store " + event.target);
                // disable the type buttons if not in the resulting grid view
            });

            topic.subscribe("fileTypeMemory_updated", function(event) {
                // fire this event after all of the filetypes have had their checked attribute updated (ie. rebuilding members after a grid search)
                var filter = new typeStore.Filter();
                var checkedFilter = filter.ne('checked', false);

                var file_types = [];
                typeStore.filter(checkedFilter).forEach(function(e) {
                    file_types.push(e.file_type);
                });

                var filt = new mainFileMemoryStore.Filter();
                eDocFilters.fileTypeFilter = filt.in('file_type', file_types);
                console.log("file type filter update complete");
                updateFilteredDisplay(mainGrid, mainFileMemoryStore, mainAssignmentMemoryStore, eDocFilters, nameStore, typeStore, gridStore).then(function(e) {
                    console.log(e);
                });
            });

            gridStore.on('update', function(event) {
                alert("the grid_cell memory store has been updated " + event.target);
                // disble the type buttons if not in the resulting grid view
            });

//            Grid Events
            mainGrid.on('dgrid-error', function(err) {
                console.log(err);
            });

			mainGrid.on('dgrid-refresh-complete', function(event) {
                console.log(event);
			});

            mainGrid.collection.on('add, update, delete', function(event) {
                 alert("files have been added, updated, or deleted into main Memory " + event.target);
             });

            mainGrid.on('dgrid-select', function(event) {
                console.log(event.rows);
            });

    //      Map Events
            var webmap = this.map;

            var mapWindow= webmap.infoWindow;

            var listener = on(mapWindow, 'selection-change', function() {

                var index = mapWindow.selectedIndex;
                if (index >= 0) {
                    var item = mapWindow.features[index];
                    console.log(item.attributes);
                    var targetReg = /GRID$/;
                    var title = item._layer.name;
                    var upperCase = title.toUpperCase();
                    if (targetReg.test(upperCase)) {
                        if (item.attributes.hasOwnProperty("GRID")) {
                            console.log(item.attributes.GRID);
                            registry.byId('_comboBoxGrid').set("displayedValue", item.attributes.GRID);

                        }
                    }
                }
            });



        },

        onOpen: function() {
           this.inherited(arguments);
        },

        onClose: function() {
            this.inherited(arguments);
        },

        startup: function () {
            this.inherited(arguments);
            this.resize();
        },

        resize: function () {
            this.inherited(arguments);

            try {
                domStyle.set(dojo.byId('apFilters'), {
                    width: '100%'
                });

                registry.byId('apFilters').resize();

                domStyle.set(dojo.byId('gridPane'), {
                    width: '100%'
                });

                registry.byId('gridPane').resize();
                registry.byId('bcMain').resize();
            }
            catch(e) {
                console.log("unable to resize widget in DOM :: "+e);
            }

        }

  });
});
