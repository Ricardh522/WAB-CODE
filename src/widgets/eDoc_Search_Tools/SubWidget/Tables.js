define([
        "dojo/_base/declare",
        "dojo/_base/array",
        'dojo/Deferred',
        'dojo/query',
        "dojo/on",
        "dojo/_base/lang",
        'dojo/promise/all',
        'esri/urlUtils',
       
        'dgrid/extensions/DijitRegistry',
        'dstore/Rest',
        "dstore/RequestMemory",
        "dstore/SimpleQuery",
        "dstore/Trackable",
        "dstore/Memory",
        'dijit/_WidgetBase',
      
    ], function (declare, Array, Deferred, query, on, lang, all, urlUtils, DijitRegistry, Rest, RequestMemory, SimpleQuery, Trackable, Memory, _WidgetBase) {

        // All of the data is retrieved and put into a RequestMemory Store with Track and Rest
        // When creating each Grid, Apply filters to the TrackableRest and create a new collection
    var rebuildMemStore = function(store) {
        var deferred = new Deferred();
        store.fetch().then(function(arr) {
            var mem = new declare([Memory, Trackable], {
                data: arr,
                idProperty: 'id'
            });
            mem.track();
            deferred.resolve(mem);
        });
        return deferred.promise;
    };

    return declare('_Tables', [_WidgetBase], {
                
                
        baseClass: 'Panel',
    
        constructor: function() {
            this.inherited(arguments);
        },

        startup: function() {
       
            var deferred = new Deferred();
            
            ///////////////////////////////////////////
            // Create the Tracking Store Rest Store////
            ///////////////////////////////////////////
            var TrackableRest = new declare([Rest, SimpleQuery, Trackable]);
            var restStore = new TrackableRest({
                target: 'https://aroragis.cloudapp.net:8443/files/',
                useRangeHeaders: true,
                ascendingPrefix: '',
                sortParam: 'ordering',
                idProperty: 'pk'
            });
            restStore.track();
            
            
            var assignmentStore = new TrackableRest({
                target: 'https://aroragis.cloudapp.net:8443/assignments/',
                useRangeHeaders: true,
                sortParam: 'ordering',
                ascendingPrefix: '',
                idProperty: 'pk'
            });
            assignmentStore.track();
           
            var stores = [{'fileArray': restStore}, {'assignArray': assignmentStore}];
            var fetch_arrays = Array.map(stores, function(e) {
                var deferred2 = new Deferred();
                var key = Object.getOwnPropertyNames(e)[0];
                var store = e[key];
                store.fetch().then(function(array) {
                    e[key] = array;
                    deferred2.resolve(e);
                });
                return deferred2.promise;
            });
                
            all(fetch_arrays).then(function(arr) {
            
                    deferred.resolve({
                        "assignArray": arr[1].assignArray,
                        "assignStore": assignmentStore,
                        "restStore": restStore,
                        "fileArray": arr[0].fileArray,
                     
                    });
                }, function(err) {
                
                deferred.resolve({
                        "assignArray": [],
                        "assignStore": assignmentStore,
                        "restStore": restStore,
                        "fileArray": [],
                });
            });
            
            return deferred.promise;
        }
    }); 
});