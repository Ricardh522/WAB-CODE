define([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/on',
	'dojo/dom',
    'dojo/has',
    'dojo/Deferred',
    'dijit/form/TextBox',
    'dstore/Memory',
    'dstore/Trackable',
	'dgrid/OnDemandGrid',
    'dgrid/List',
	'dgrid/extensions/ColumnReorder',
	'dgrid/extensions/ColumnResizer',
	'dgrid/extensions/ColumnHider',
	'dgrid/Keyboard',
	'dgrid/Selection',
    'dgrid/editor',
	'dgrid/extensions/DnD',
	'dojo/dnd/Source',
	'dgrid/extensions/DijitRegistry'
], function (declare, lang, on, dom, has, Deferred, TextBox, Memory, Trackable, OnDemandGrid, List, ColumnReorder, ColumnResizer, ColumnHider, Keyboard, Selection, editor, DnD, DnDSource, DijitRegistry) {
    
    var createColumns = function() {
        return [
                {
                    field: 'pk',
                    label: 'Primary Key',
                    reorderable: false,
                    resizable: true,
                    hidden: true,
                    unhidable: false
                },
                {
					field: 'base_name',
					label: 'File Name',
					reorderable: false,
					resizable: true,
                    hidden: false,
					unhidable: true
				},
				{
					field: 'file_type',
					label: 'File Type',
					reorderable: true,
					resizable: true,
					hidden: true,
					unhidable: false
				},
				{
					field: 'size',
					label: 'File Size',
					reorderable: true,
					resizable: true,
					hidden: true,
					unhidable: false
				},
				{
					field: 'grid_cells',
					label: 'Grid Cell(s)',
					reorderable: true,
					resizable: true,
					hidden: false
				},
				editor({
					field: 'comment',
					editor: 'TextBox',
					editOn: has('touch') ? 'click' : 'dblclick',
                    autoSelect: true,
                    autoSave: true,
					label: 'Comments',
					reorderable: true,
					resizable: true,
					hidden: false
				}),
				{
					field: 'date_added',
					label: 'Date Added',
					reorderable: true,
					resizable: true,
					hidden: true
				},
				{
					field: 'mime',
					label: 'MIME Type',
					reorderable: true,
					resizable: true,
					hidden: true
				},
				{
					field: 'file_path',
					label: 'File Path',
					reorderable: true,
					resizable: true,
					hidden: true
				}	
            ];
    };
    
    var customGrid = declare([OnDemandGrid, Keyboard, Selection, editor, ColumnReorder, ColumnResizer, ColumnHider, DijitRegistry]);
           
	return declare(customGrid, {
        
		postCreate: function() {
			this.inherited(arguments);
			this.on('dgrid-error', function(event) {
                alert('error');
            });
		},
		
		constructor: function(fileArray) {
            this.collection = new declare([Memory, Trackable], {
                data: fileArray,
                idProperty: 'pk'
            });
            this.id = "main_dgrid";
			this.loadingMessage = 'Loading Data';
            this.noDataMessage = 'No results found.';
			this.selectionMode = "single";
            this.dndSourceType = "grid-row";
			this.allowSelectAll = true;
            this.cellNavigation = true;
            this.pageSkip = 15;
			this.columns = createColumns();
        }
	});

});

