define(['dojo/_base/declare', 'dojo/Evented', 'dijit/_WidgetsInTemplateMixin', 'jimu/BaseWidget', './SubWidget/SubWidget'],
function(declare, Evented, _WidgetsInTemplateMixin, BaseWidget, SubWidget) {
  //To create a widget, you need to derive from BaseWidget.
  return declare([BaseWidget, _WidgetsInTemplateMixin], {

    // Custom widget code goes here

    baseClass: 'e-dock',
    // this property is set by the framework when widget is loaded.
    // name: '_eDock',
    // add additional properties here

    //methods to communication with app container:
    postCreate: function() {
      this.inherited(arguments);
      console.log('_eDock::postCreate');
      this.subWidget = new SubWidget({
        map: this.map,
        config: this.config,
        id: 'eDoc'
      });
      this.subWidget.placeAt(this.domNode);
      
    },

    startup: function () {
      this.inherited(arguments);
      console.log('_eDock::startup');
      // this.logMessage("startup");
      this.subWidget.startup();
    },

     onOpen: function () {
         this.inherited(arguments);
         console.log('_eDock::onOpen');
         this.subWidget.onOpen();
     },

     onClose: function(){
       console.log('_eDock::onClose');
         this.subWidget.onClose();
     },

    // onMinimize: function(){
    //   console.log('_eDock::onMinimize');
    // },

     onMaximize: function(){
       console.log('_eDock::onMaximize');
       this.subWidget.resize();
     },

    // onSignIn: function(credential){
    //   console.log('_eDock::onSignIn', credential);
    // },

    // onSignOut: function(){
    //   console.log('_eDock::onSignOut');
    // }

     onPositionChange: function () {
        this.inherited(arguments);
        console.log('_eDock::onPositionChange');
     },

     resize: function () {
       this.inherited(arguments);
       this.subWidget.resize();
       console.log('_eDock::resize');
     }

//methods to communication between widgets:

    // logMessage: function(message) {
    //   this.logNode.innerHTML = this.logNode.innerHTML + '<br>'
    // }


  });

});
