module.exports = function(grunt) {

  // replace these with your own paths
  var appDir = 'C:/WAB-CODE/WebAppBuilderForArcGIS/server/apps/4';
  var stemappDir = 'C:/WAB-CODE/WebAppBuilderForArcGIS/client/stemapp';
  grunt.initConfig({

    watch: {
      main: {
        files: ['src/**', 'tests/**'],
        tasks: ['jshint', 'sync'],
        options: {
          spawn: false
        }
      }
    },

    sync: {
      main: {
        files: [{
          cwd: 'src',
          src: ['**'],
          dest: appDir
        }, {
          cwd: 'src',
          src: ['**'],
          dest: stemappDir
        }],
        verbose: true // Display log messages when copying files
      }
    },

    browserSync: {

      bsFiles: {
        src: [
        '/src/widgets/**/*.js',
        '/src/widgets/**/*.html',
        '/src/widgets/**/*.css'
        ]
      },
      options: {
        ui: {
          port: 8080
        },
        server: {
         browser: "google chrome",
         directory: false,
         baseDir: './'
        },
        watchTask: true
      }
    },

    jshint : {
      options: {
        reporter: require('jshint-stylish'),
        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        dojo: true
      },

      all: ['gruntfile.js', 'src/widgets/eDoc_Search_Tools/**/*.js', 'tests/**/*.js']
    }

  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.registerTask('default', ['sync', 'watch']);
  grunt.registerTask('serve', ['jshint', 'sync', 'browserSync', 'watch']);
};
