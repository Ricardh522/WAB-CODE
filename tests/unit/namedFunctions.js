define([
    "intern!bdd", 
    "intern/chai!expect",
    
    "esri/arcgis/OAuthInfo",
    "esri/IdentityManager",
    "esri/arcgis/utils",
    "esri/config",
    "esri/request",
    
    "dijit/registry",
    
    'dojo/dom-construct',
    "dojo/Deferred",
    "dojo/promise/all",
    "dojo/json",
    "dojo/_base/declare",
    "dojo/_base/array",
    
    "dstore/Memory",
    'dstore/Trackable',

    "eDoc_Search_Tools/SubWidget/SubWidget"
], function(bdd, expect, OAuthInfo, esriId, arcgisUtils, esriConfig, esriRequest,
 registry, domConstruct, Deferred, all, JSON, declare, Array, Memory, Trackable,
WidgetUnderTest) {
    
        bdd.describe("test suite for SubWidget's named functions", function() {
             var widget; 
             var map;
             var unfilteredFileArray;
             var unfilteredAssignmentArray;

             var destroy = function (widget) {
                registry.forEach(function(e) {
                    registry.remove(e.id);
                });
                 
                if (widget && widget.destroyRecursive) {
                    
                    widget.destroyRecursive();
                    widget = null;
                }
                
            };  
            
            bdd.before(function() {
                var mainDeferred = new Deferred();
                var deferred1 = new Deferred();
                var deferred2 = new Deferred();
                var deferred3 = new Deferred();

                var deferreds = [deferred1, deferred2, deferred3];

                esriConfig.defaults.io.corsEnabledServers.push('http://edoc.azurewebsites.net');

               var geometryService = "http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer";
                var request1 = {
                    url: '/tests/unit/fixtures/webmap.json',
                    content: {f: 'json', token:"vm7Ztgq6NgodQs-Qn-rNArKEgVivGbtp3Iwq47UmbMslOszN8Q9qnHeYq1J4J5DAX-1spxEdPjh91NUwxeluAw.."},
                    handleAs: 'json'
                };
                var mapid = "6c62f8f79a76447cb2e3f87ed128d3fc";
                // esriRequest(request1).then(function(response) {
                //     arcgisUtils.createMap(response, domConstruct.create("div"), {
                //         geometryServiceURL: geometryService
                //     })
                //         .then(function(response2) {
                //             map = response2.map;
                //             deferred1.resolve(map);
                //     }, function(err) {
                //         console.log(err);
                //         });
                // }, function(err) {
                //     console.log(err);
                // });
                arcgisUtils.createMap(mapid, domConstruct.create("div"), {
                        geometryServiceURL: geometryService
                    })
                        .then(function(response2) {
                            map = response2.map;
                            deferred1.resolve(map);
                    }, function(err) {
                        console.log(err);
                        });

                var request2 = {
                    url: '/tests/unit/fixtures/assignmentRestStore.json',
                    content: {f: 'json'},
                    handleAs: 'json'
                };
                esriRequest(request2).then(function(response) {
                    unfilteredAssignmentArray = response; 
                    deferred2.resolve(response);
                });


                var request3 = {
                    url: '/tests/unit/fixtures/fileRestStore.json',
                    content: {f: 'json'},
                    handleAs: 'json'
                };
                esriRequest(request3).then(function(response) {
                    unfilteredFileArray = response;
                    deferred3.resolve(response);
                });

                all(deferreds).then(function(array) {
                    mainDeferred.resolve(array);
                });

                return mainDeferred.promise;
                
            });
            
            bdd.beforeEach(function() {
                widget = new WidgetUnderTest({map: map}, domConstruct.create('div', null));
            });

            bdd.afterEach(function() {
                destroy(widget);
            });

            bdd.after(function() {
                destroy(map);
            });
            
            bdd.describe('createColumns', function() {
                bdd.it('should create columns for the table ', function() {
                    var columns = widget.createColumns();
                    expect(columns).to.be.an('array');
                });
            });

            bdd.describe('createMemStore', function() {
                bdd.it('should create a new Trackable Memory Store', function() {
                    var store = new declare([Memory, Trackable]);
                    var data = unfilteredFileArray;
                    var idProperty = 'pk';
                    var newStore = widget.createMemStore(store, data, idProperty);
                    expect(newStore).itself.to.respondTo('fetch');
                    expect(newStore).itself.to.respondTo('track');
                
                });
            });

            bdd.describe('createFileNameFixture', function() {
                
                bdd.it('should create a filename fixture', function() {
                    var inputArray = unfilteredFileArray;
                    var fixture = widget.createFileNameFixture(inputArray); 
                    expect(fixture).to.be.an('array');
                    expect(fixture).to.have.length.above(0);
                    Array.forEach(fixture, function(e) {
                        expect(e).to.have.all.keys(['base_name']);
                    });
                });

                bdd.it('should not have duplicate fileNames in the fixture', function() {
                    var inputArray = unfilteredFileArray;
                    var fixture = widget.createFileNameFixture(inputArray); 
                    var counts = [];
                    Array.forEach(fixture, function(e) {
                        var base_name = e.base_name;
                        expect(counts).not.to.include(base_name);
                        if (counts.indexOf(base_name) === -1) {
                            counts.push(base_name);
                        }
                    });
                });
            });

            bdd.describe('createFileTypeFixture', function() {
                
                bdd.it('should create a filetype fixture', function() {
                    var inputArray = unfilteredFileArray;
                    var fixture = widget.createFileTypeFixture(inputArray);
                    expect(fixture).to.be.an('array');
                    expect(fixture).to.have.length.above(0);
                    Array.forEach(fixture, function(e) {
                        expect(e).to.have.all.keys(['file_type', 'checked']);
                    });
                });

                bdd.it('should not have duplicate file types in fixture', function() {
                    var inputArray = unfilteredFileArray;
                    var fixture = widget.createFileTypeFixture(inputArray);
                    var counts = [];
                    Array.forEach(fixture, function(e) {
                        var file_type = e.file_type;

                        expect(counts).not.to.include(file_type);
                        if (counts.indexOf(file_type) === -1) {
                            counts.push(file_type);
                        }
                    });
                });
            });

            bdd.describe('createGridCellFixture', function() {

                bdd.it('should return a grid cell fixture', function() {
                    var inputArray = unfilteredAssignmentArray;
                    var fixture = widget.createGridCellFixture(inputArray);
                    expect(fixture).to.be.an('array');
                    expect(fixture).to.have.length.above(0);
                    Array.forEach(fixture, function(e) {
                        expect(e).to.have.all.keys(['grid_cell']);
                    });
                });
                
                bdd.it('should not have duplicate gridcells in the fixture', function() {
                    var inputArray = unfilteredAssignmentArray;
                    var fixture = widget.createGridCellFixture(inputArray);
                    var counts = [];
                    Array.forEach(fixture, function(e) {
                        var grid_cell = e.grid_cell;
                        expect(counts).not.to.include(grid_cell);
                        if (counts.indexOf(grid_cell) === -1) {
                            counts.push(grid_cell);
                        }
                    });
                });
            });

            bdd.describe('updateFileTypeCount', function() {
                bdd.it('should update the File Type Store checked property to false', function() {
                    var myFileTypeMemory = new declare([Memory, Trackable]);
                    var array = widget.createFileTypeFixture(unfilteredFileArray);
                    var typeStore = widget.createMemStore(myFileTypeMemory, array, 'file_type');
                  
                    var val = false;
                    var label = "PDF";

                    widget.updateFileTypeCount(typeStore, val, label);
                    var after_update = typeStore.getSync(label);
                   
                    expect(after_update).to.have.property('checked', false);
                });

                bdd.it('should update the File Type Store filetype to true', function() {
                    var myFileTypeMemory = new declare([Memory, Trackable]);
                    var array = widget.createFileTypeFixture(unfilteredFileArray);
                    var typeStore = widget.createMemStore(myFileTypeMemory, array, 'file_type');

                    var label = "PDF";
                    var count = 10;

                    widget.updateFileTypeCount(typeStore, count, label);
                    var after_update = typeStore.getSync(label);
                   
                    expect(after_update.checked).to.equal(10);
                });
            });

            bdd.describe('getCount', function() {
                bdd.it('should return the frequency of an object attribute in an array of objects', function() {
                    var attribute = 'file_type';
                    var value = 'PDF';
                    var array = unfilteredFileArray;
                    var occurances = widget.getCount(attribute, value, array);
                    expect(occurances).to.equal(183);
                });
            });

            bdd.describe('buildFileTypeButtons', function() {
                bdd.it('should return an object with children buttons', function() {
                    var myFileTypeMemory = new declare([Memory, Trackable]);
                    var array = widget.createFileTypeFixture(unfilteredFileArray);
                    var typeStore = widget.createMemStore(myFileTypeMemory, array, 'file_type');
                    typeStore.fetch().then(function(arr) {
                        Array.forEach(arr, function(e) {
                            if (registry.byId(e.file_type)) {
                                registry.remove(e.file_type);
                            }
                        });
                        widget.buildFileTypeButtons(typeStore, unfilteredFileArray).then(function(e) {
                            Array.forEach(arr, function(e) {
                                expect(registry.byId(e.file_type)).to.be.an('object');
                            });
                        });
                    }); 
                });
            });

            bdd.describe('pollSelection', function() {
                bdd.it('should create a checkbox for each entry in the selectStore', function() {
                    var request = {
                        url: '/tests/unit/fixtures/selectionStore.json',
                        content: {f: 'json'},
                        handleAs: 'json'
                    };
                    var mainDeferred = new Deferred();

                    esriRequest(request).then(function(response) {
                        var mySelectionMemory = new declare([Memory, Trackable]);
                        var selectStore = widget.createMemStore(mySelectionMemory, response, 'pk');
                        widget.pollSelection(selectStore);
                        selectStore.fetch().then(function(array) {
                            var revs = Array.map(array, function(e) {
                                var deferred = new Deferred();
                                var box = registry.byId(e.pk + '_box');
                                expect(box).to.be.an('object');
                                expect(box).to.have.property('checked', true);
                                return deferred.resolve(e);
                            });

                            all(revs).then(function(e) {
                                mainDeferred.resolve(e);
                            });
                        });
                    });

                    return mainDeferred.promise;
                });
            });

            bdd.describe('buildActionButtons', function() {
                bdd.it('should return a <div> with children', function() {
                    var deferred = new Deferred();
                    var request = {
                        url: '/tests/unit/fixtures/selectionStore.json',
                        content: {f: 'json'},
                        handleAs: 'json'
                    };

                    if (registry.byId('_downloadForm')) {
                        registry.remove('_downloadForm');
                    }

                    esriRequest(request).then(function(response) {
                        var mySelectionMemory = new declare([Memory, Trackable]);
                        var selectionStore = widget.createMemStore(mySelectionMemory, response, 'pk');
                            widget.buildActionButtons(selectionStore).then(function(e) {
                                expect(e).to.have.property('childNodes');
                                expect(e.childNodes).to.have.length.above(0);
                                deferred.resolve(e);
                            });
                        });
                    return deferred.promise;
                });
            });

            bdd.describe('assignCell2File', function() {
                bdd.it("should return an array", function() {
                    var mainDeferred = new Deferred();
                    var grid = widget.mainGrid;
                    var grid_cell = 'A2';
                    var rows = [1,2,3,4,5,6,7];
                    Array.forEach(rows, function(e) {
                        grid.select(e);
                    });

                    var assignmentRestStore = widget.assignmentRestStore;
                    var backend_url = 'http://localhost:8003';
                    widget.assignCell2File(backend_url, grid, grid_cell, assignmentRestStore).then(function(e) {
                        expect(e).to.be.an('array');
                        mainDeferred.resolve(e);
                    });
                    return mainDeferred.promise;
                });
            });

            bdd.describe('removeCellFromFile', function() {
                bdd.it('should return an array', function() {
                    var mainDeferred = new Deferred();
                    var grid = widget.mainGrid;
                    var grid_cell = 'A2';
                    var rows = [1,2,3,4,5,6,7];
                    Array.forEach(rows, function(e) {
                        grid.select(e);
                    });

                    var assignmentRestStore = widget.assignmentRestStore;
                    var backend_url = 'http://localhost:8003';
                    var mainAssignmentMemoryStore = widget.mainAssignmentMemoryStore;
                    widget.removeCellFromFile(backend_url, grid, mainAssignmentMemoryStore, grid_cell,
                        assignmentRestStore).then(function(e) {
                            expect(e).to.be.an('array');
                            mainDeferred.resolve(e);
                        });
                    return mainDeferred.promise;
                });
            });

            bdd.describe('updateFilteredDisplay', function() {
                bdd.it('should return an array', function() {
                    var mainDeferred = new Deferred();

                    var mainGrid = widget.mainGrid;
                    var mainFileMemoryStore = widget.mainFileMemoryStore;
                    var mainAssignmentMemoryStore = widget.mainAssignmentMemoryStore;
                    var filters = widget.filterObjects;
                    var nameStore = widget.nameStore;
                    var typeStore = widget.typeStore;
                    var gridStore = widget.gridStore;

                    widget.updateFilteredDisplay(mainGrid, mainFileMemoryStore, mainAssignmentMemoryStore,
                        filters, nameStore, typeStore, gridStore).then(function(e) {
                            expect(e).to.be.an('array');
                            expect(e.length).to.equal(2);
                            var fileArray = e[0];
                            expect(fileArray[0]).to.have.all.keys('pk', 'file_path', 'base_name',
                                    'grid_cells', 'file_type', 'size', 'date_added', 'mime', 'comment');
                                
                            var assignArray = e[1];
                            expect(assignArray[0]).to.have.all.keys('pk', 'grid_cell', 'file', 'base_name', 'date_assigned',
                                    'comment');
                            mainDeferred.resolve(e);
                        
                        });


                    return mainDeferred.promise;


                });
            });

            bdd.describe('resetMainAssignmentMemoryStore', function() {
                bdd.it('should return a file Store Array and set data on the mainAssignmentMemoryStore', function() {
                    var mainAssignmentMemoryStore = widget.mainAssignmentMemoryStore;
                    mainAssignmentMemoryStore.setData([]);
                    var assignmentRestStore = widget.assignmentRestStore;
                    widget.resetMainAssignmentMemoryStore(mainAssignmentMemoryStore, assignmentRestStore).then(function(e) {
                        expect(e).to.be.an('array');
                        widget.mainAssignmentMemoryStore.fetch().then(function(e) {
                            expect(e.length).to.be.above(0);
                        });
                    });
                });
            });

            bdd.describe('resetMainFileMemoryStore', function() {
                bdd.it('should return an unfiltered file store Array and set data on the type store', function() {
                    var mainFileMemoryStore = widget.mainFileMemoryStore;
                    mainFileMemoryStore.setData([]);
                    var fileRestStore = widget.fileRestStore;
                    var typeStore = widget.typeStore;
                    typeStore.setData([]);
                    var preTypeLength = typeStore.data.length;
                    widget.resetMainFileMemoryStore(mainFileMemoryStore, fileRestStore, typeStore).then(function(e) {
                        expect(e).to.be.an('array');
                        typeStore.fetch().then(function(e) {
                            expect(e.length).to.be.above(preTypeLength);
                        });
                    });
                });
            });

            bdd.describe('fetchRestStores', function() {
                bdd.it('should return an array with a fileArray and an assignmentArray', function() {
                    var fileRestStore = widget.fileRestStore;
                    var assignmentRestStore = widget.assignmentRestStore;
                    var mainFileMemoryStore = widget.mainFileMemoryStore;
                    var mainAssignmentMemoryStore = widget.mainAssignmentMemoryStore;
                    var typeStore = widget.typeStore;

                    widget.fetchRestStores(fileRestStore, assignmentRestStore, mainFileMemoryStore,
                        mainAssignmentMemoryStore, typeStore).then(function(e) {
                            expect(e).to.be.an('array');
                            expect(e.length).to.equal(2);
                            Array.forEach(e, function(array) {
                                expect(array.length).to.be.above(0);
                            });
                        });

                });
            });

            bdd.describe('cleanFilters', function() {
                bdd.it('should return a new filters Object', function() {
                    var mainFileMemoryStore = widget.mainFileMemoryStore;
                    var mainAssignmentMemoryStore = widget.mainAssignmentMemoryStore;
                    var filterObjects = widget.filterObjects;
                    var newFilterObj = widget.cleanFilters(filterObjects, mainFileMemoryStore, mainAssignmentMemoryStore);
                    expect(newFilterObj).to.be.an('object');
                    expect(newFilterObj).to.have.all.keys(['fileNameFilter', 'fileTypeFilter', 'fileCellFilter',
                        'assignCellFilter', 'assignNameFilter']);
                });
            });

            bdd.describe('rebuildMembers', function() {
                bdd.it('should set localStorage fileArray and assignArray', function() {
                    var request = {
                        url: '/tests/unit/fixtures/fileRestStore.json',
                        content: {f: 'json'},
                        handleAs: 'json'
                    };
                    
                    var mainDeferred = new Deferred();
                   
                    var fileArray= esriRequest(request).then(function(response) {
                        var deferred = new Deferred();
                        deferred.resolve(response);
                        return deferred.promise;
                    });

                    var request1 = {
                        url: '/tests/unit/fixtures/assignmentRestStore.json',
                        content: {f: 'json'},
                        handleAs: 'json'
                    };

                    var assignArray = esriRequest(request1).then(function(response) {
                        var deferred = new Deferred();
                        deferred.resolve(response);
                        return deferred.promise;
                    });    

                    all([fileArray, assignArray]).then(function(e) {
                        var fileArray = e[0];
                        var assignArray = e[1];
                        var nameStore = widget.nameStore;
                        var typeStore = widget.typeStore;
                        var gridStore = widget.gridStore;

                        widget.rebuildMembers(fileArray, assignArray, nameStore, typeStore, gridStore).then(function(e) {
                            var fileArray = JSON.parse(localStorage.fileArray);
                            expect(fileArray).to.be.an('array');
                            expect(fileArray.length).to.be.above(0);
                            var assignArray = JSON.parse(localStorage.assignArray);
                            expect(assignArray).to.be.an('array');
                            expect(assignArray.length).to.be.above(0);
                            mainDeferred.resolve(e);
                        });
                    });

                    return mainDeferred.promise;
                });
            });

            bdd.describe('renewFromRest', function() {
                bdd.it('should return an array of filtered fileArray and assignArray', function() {
                    var fileRestStore = widget.fileRestStore;
                    var assignmentRestStore = widget.assignmentRestStore;
                    var mainFileMemoryStore = widget.mainFileMemoryStore;
                    var mainAssignmentMemoryStore = widget.mainAssignmentMemoryStore;
                    var typeStore = widget.typeStore;
                    var mainGrid = widget.mainGrid;
                    var eDocFilters = widget.filterObjects;
                    var nameStore = widget.nameStore;
                    var gridStore = widget.gridStore;

                    widget.renewFromRest(fileRestStore, assignmentRestStore, mainFileMemoryStore,
                        mainAssignmentMemoryStore, typeStore, mainGrid, eDocFilters, nameStore, gridStore).then(function(e) {
                            expect(e).to.be.an('array');
                        });
                });
            });
        });
});