define([
    "intern!bdd", 
    "intern/chai!expect",
    
    "esri/arcgis/OAuthInfo",
    "esri/IdentityManager",
    "esri/arcgis/utils",
    "esri/config",
    "esri/request",
    
    "dijit/registry",
    
    'dojo/dom-construct',
    "dojo/Deferred",
    "dojo/json",
    "dojo/_base/declare",
    "dojo/_base/array",
    
    "eDoc_Search_Tools/SubWidget/SubWidget"
],
    
       function(bdd, expect, OAuthInfo, esriId, arcgisUtils, esriConfig, esriRequest, registry, domConstruct, Deferred, JSON, declare, Array, WidgetUnderTest) {
    
        bdd.describe("test suite for SubWidget/SubWidget", function() {
             var widget; 
             var destroy = function (widget) {
                registry.forEach(function(e) {
                    registry.remove(e.id);
                });
                 
                if (widget && widget.destroyRecursive) {
                    
                    widget.destroyRecursive();
                    widget = null;
                }
                
            };  
            var map;
            var customGrid;
            var memoryStore;
            
            
            bdd.before(function() {
                var deferred = new Deferred();
              
                var geometryService = "http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer";
                var request = {
                    url: '/tests/unit/fixtures/webmap.json',
                    content: {f: 'json', token:"vm7Ztgq6NgodQs-Qn-rNArKEgVivGbtp3Iwq47UmbMslOszN8Q9qnHeYq1J4J5DAX-1spxEdPjh91NUwxeluAw.."},
                    handleAs: 'json'
                };
                var mapid = "6c62f8f79a76447cb2e3f87ed128d3fc";
                // esriRequest(request).then(function(response) {
                //     arcgisUtils.createMap(response, domConstruct.create("div"), {
                //         geometryServiceURL: geometryService
                //     })
                //         .then(function(response2) {
                //             map = response2.map;
                //             deferred.resolve(map);
                //     }, function(err) {
                //         console.log(err);
                //         });
                // }, function(err) {
                //     console.log(err);
                // });
                arcgisUtils.createMap(mapid, domConstruct.create("div"), {
                        geometryServiceURL: geometryService
                    })
                        .then(function(response2) {
                            map = response2.map;
                            deferred.resolve(map);
                    }, function(err) {
                        console.log(err);
                        });
                return deferred.promise;
            });
            
            bdd.beforeEach(function() {
                widget = new WidgetUnderTest({map: map}, domConstruct.create('div', null));

            });

            bdd.afterEach(function() {
                destroy(widget);
              });

            bdd.after(function() {
                destroy(map);
            });
            
            bdd.describe('Sanity', function() {
                bdd.it('should create a subwidget', function() {
                    expect(widget).to.be.an.instanceof(WidgetUnderTest);
                });
            });
            
             bdd.describe('Main Grid', function() {
                bdd.it('should have a mainGrid Object', function() {
                    expect(widget.mainGrid).to.have.property('collection')
                    .that.is.an('object');
                    console.log({"mainGrid.Collection": widget.mainGrid.collection});
                });
            });
                
            bdd.describe('Filters', function() {
                bdd.it('should have filter Objects defined', function() {
                    expect(widget.filterObjects).to.be.an('object');
                });
            });
                
           
            bdd.describe('Rest Stores', function() {
                bdd.describe('fileRestStore', function() {
                    bdd.it('should have a File Rest Store', function() {
                        expect(widget.fileRestStore).to.have.property('idProperty').that.equals('pk');
                    });
                });

                bdd.describe('assignmentRestStore', function() {
                    bdd.it('should have an Assignment Rest Store', function() {
                        expect(widget.assignmentRestStore).to.have.property('idProperty').that.equals('pk');
                    });
                });
            });

            bdd.describe('Memory Stores', function() {
                
                bdd.describe('mainFileMemoryStore', function() {
                    bdd.it('should have a main File Memory store', function() {
                        expect(widget.mainFileMemoryStore).to.have.property('data')
                        .that.is.an('array');
                        expect(widget.mainFileMemoryStore).to.have.property('idProperty')
                        .that.equals('pk');
                        console.log({"mainFileMemoryStore": widget.mainFileMemoryStore.data});
                    });
                });
                
                bdd.describe('fileType Store', function() {
                    bdd.it('should have a file type Store', function() {
                        expect(widget.typeStore).to.have.property('data')
                        .that.is.an('array');
                        expect(widget.typeStore).to.have.property('idProperty')
                        .that.equals('file_type');
                        console.log({"fileTypeStore": widget.typeStore.data});
                    });

                    bdd.it('should not have duplicate file types in store', function() {
                        widget.typeStore.fetch().then(function(arr) {
                            console.log(arr);
                            var counts = [];
                            Array.forEach(arr, function(e) {
                                var file_type = e.file_type;

                                expect(counts).not.to.include(file_type);
                                if (counts.indexOf(file_type) === -1) {
                                    counts.push(file_type);
                                }
                            });
                        });
                    });
                });
                
                bdd.describe('fileName Store', function() {
                    bdd.it('should have a filename store', function() {
                        expect(widget.nameStore).to.have.property('data')
                        .that.is.an('array');
                        expect(widget.nameStore).to.have.property('idProperty')
                        .that.equals('base_name');
                        console.log({"fileNameStore": widget.nameStore.data});
                    });
                });
                
                bdd.describe('gridCellStore', function() {
                    bdd.it('should have a grid cell store', function() {
                        expect(widget.gridStore).to.have.property('data')
                        .that.is.an('array');
                         expect(widget.gridStore).to.have.property('idProperty')
                        .that.equals('grid_cell');
                        console.log({"gridCellStore": widget.gridStore.data});
                    });

                    bdd.it('should not have duplicate grid cells in store', function() {
                        widget.gridStore.fetch().then(function(arr) {
                            var counts = [];
                            Array.forEach(arr, function(e) {
                                var cell = e.grid_cell;

                                expect(counts).not.to.include(cell);
                                    if (counts.indexOf(cell) === -1) {
                                        counts.push(cell);
                                    }
                            });
                        });
                    });
                });
                    
              
                bdd.describe('selectionStore', function() {
                    bdd.it('should have a selection store', function() {
                        expect(widget.selectionStore).to.have.property('data')
                        .that.is.an('array');
                        expect(widget.selectionStore).to.have.property('idProperty')
                        .that.equals('pk');
                        console.log({"selectionStore": widget.selectionStore.data});
                    });
                });
            });
                
        });
           
});
