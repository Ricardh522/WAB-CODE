define([
    'intern!object',
    'intern/chai!assert'
], function(registerSuite, assert) {
    registerSuite({
        name: 'index',
        'testing_page': function() {
            return this.remote
            .get(require.toUrl('src/widgets/eDoc_Search_Tools/test_page/TestSubWidget.html'))
            .setFindTimeout(5000);
            
        }
    });
});